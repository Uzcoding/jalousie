$(function () {

    $(window).scroll(function(){
        if ( $(this).scrollTop() > 0 ) {
            $('.header').addClass('fixed');
        } else {
            $('.header').removeClass('fixed');
        }
         
    });

    $("[data-scroll]").on("click", function (event) {
		event.preventDefault();

		var $this = $(this),
		 	blockId = $(this).data('scroll'),
			blockOffset = $(blockId).offset().top;


		$("html, body").animate({
			scrollTop: blockOffset
		}, 2000);
    });
    

})